	var express = require('express'); 
    var Request = require('request');
    var app = express(); 
    var bodyParser = require('body-parser');
    var multer = require('multer');
    var xlstojson = require("xls-to-json-lc");
    var xlsxtojson = require("xlsx-to-json-lc");
    var jsonMain = null;
    var jsonStringMain = '{\n' +
        '  "partnerIdentifier": "LABaby",\n' +
        '  "key": "32d17a62-9947-492c-b44e-8c3ce4fc5e53",\n' +
        '  "test": true,\n' +
        '  "interchange": {\n' +
        '    "dateCreated": "2021-02-15T16:29:29.580Z",\n' +
        '    "controlNumber": "09303428790930342879"\n' +
        '  }\n' +
        '}';

    app.use(bodyParser.json());  

    var storage = multer.diskStorage({ //multers disk storage settings
        destination: function (req, file, cb) {
            cb(null, './uploads/')
        },
        filename: function (req, file, cb) {
            var datetimestamp = Date.now();
            cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
        }
    });

    var upload = multer({ //multer settings
                    storage: storage,
                    fileFilter : function(req, file, callback) { //file filter
                        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length-1]) === -1) {
                            return callback(new Error('Wrong extension type'));
                        }
                        callback(null, true);
                    }
                }).single('file');

    app.post('/upload', function(req, res) {
        var exceltojson;
        upload(req,res,function(err){
            if(err){
                 res.json({error_code:1,err_desc:err});
                 return;
            }
            if(!req.file){
                res.json({error_code:1,err_desc:"No file passed"});
                return;
            }

            if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xlsx'){
                exceltojson = xlsxtojson;
            } else {
                exceltojson = xlstojson;
            }
            console.log(req.file.path);
            try {
                exceltojson({
                    input: req.file.path,
                    output: null,
                    lowerCaseHeaders:true
                }, function(err,result){
                    if(err) {
                        return res.json({error_code:1,err_desc:err, data: null});
                    }
                    jsonMain = JSON.parse(jsonStringMain);
                    console.log(jsonMain);
                    let arrayTransactionSet                          = [];

                    for (const invoiceDate of result) {
                        if(invoiceDate.ponumber)
                        {
                            console.log(invoiceDate);
                            let transactionSet                          = {};
                            let item                                    = {};
                            transactionSet['detail']                    = {};
                            transactionSet['detail']['shipToAddress']   = {};
                            transactionSet['detail']['billToAddress']   = {};
                            let items                                   = [];

                            transactionSet.controlNumber                              = "09303428790930342879";
                            transactionSet.detail.poNumber                            = Number(invoiceDate.ponumber) ;
                            transactionSet.detail.invoiceNumber                       = invoiceDate.invoicenumber;
                            transactionSet.detail.invoiceDate                         = invoiceDate.invoicedate;
                            transactionSet.detail.invoiceDueDate                      = invoiceDate.invoiceduedate;
                            transactionSet.detail.creditMemo                          = false;//invoiceDate.creditmemo;

                            transactionSet.detail.shipToAddress.namePrimary        = invoiceDate.shipnameprimary;
                            transactionSet.detail.shipToAddress.nameSecondary      = invoiceDate.shipnamesecondary;
                            transactionSet.detail.shipToAddress.streetPrimary      = invoiceDate.shipstreetprimary;
                            transactionSet.detail.shipToAddress.treetSecondary     = invoiceDate.shipstreetsecondary;
                            transactionSet.detail.shipToAddress.city               = invoiceDate.shipcity;
                            transactionSet.detail.shipToAddress.zipCode            = invoiceDate.shipzipcode;
                            transactionSet.detail.shipToAddress.stateCode          = invoiceDate.shipstatecode;
                            transactionSet.detail.shipToAddress.countryCode        = invoiceDate.shipcountrycode;


                            transactionSet.detail.billToAddress.namePrimary        = invoiceDate.billnameprimary;
                            transactionSet.detail.billToAddress.nameSecondary      = invoiceDate.billnamesecondary;
                            transactionSet.detail.billToAddress.streetPrimary      = invoiceDate.billstreetprimary;
                            transactionSet.detail.billToAddress.treetSecondary     = invoiceDate.billstreetsecondary;
                            transactionSet.detail.billToAddress.city               = invoiceDate.billcity;
                            transactionSet.detail.billToAddress.zipCode            = invoiceDate.billzipcode;
                            transactionSet.detail.billToAddress.stateCode          = invoiceDate.billstatecode;
                            transactionSet.detail.billToAddress.countryCode        = invoiceDate.billcountrycode;

                            item.vendorItemNumber      = invoiceDate.vendoritemnumber;
                            item.quantity              = Number(invoiceDate.quantity);
                            item.quantityMeasurement   = invoiceDate.quanitmeasurment;
                            item.itemDescription       = invoiceDate.itemdescription;
                            item.unitPrice             = Number(invoiceDate.unitprice);
                            item.discount              = invoiceDate.discount ? Number(invoiceDate.discount) : 0;
                            item.total                 = Number(invoiceDate.unitprice) * Number(invoiceDate.quantity);
                            items.push(item);
                            transactionSet.detail.items = (items);
                            transactionSet.detail.termsDiscountPercent = 0;
                            transactionSet.detail.termsDiscountAmount  = 0;
                            transactionSet.detail.freight              = 0;
                            transactionSet.detail.tax                  = 0;
                            transactionSet.detail.miscellaneousFees    = 0;
                            transactionSet.detail.subTotal             = Number(invoiceDate.subtotal);
                            transactionSet.detail.total                = Number(invoiceDate.total);
                            arrayTransactionSet.push(transactionSet);
                        }


                     }
                    jsonMain.interchange.transactionSet = arrayTransactionSet;
                    var fs = require('fs');

                    fs.writeFile('converted.json', JSON.stringify(jsonMain,  null, 4), function (err) {
                        if (err) throw err;
                        console.log('Saved!');
                    });

                    Request.post({
                        "headers": { "content-type": "application/json" },
                        "url": "https://api-interchange.clarkinc.biz/VendorPortal/API/Invoice",
                        "body": JSON.stringify(jsonMain)
                    }, (error, response, body) => {
                        if(error) {
                            return console.dir(error);
                            console.log(error);
                        }
                        responseBody = JSON.parse(body)
                        if(responseBody.Succeeded)
                        {
                            console.log();


                            res.sendFile(__dirname + "/public/index.html",jsonMain);
                        }else
                        {
                            res.json(responseBody.Errors);
                        }
                    });
                });
            } catch (e){
                res.json({error_code:1,err_desc:"Corrupted excel file"});
            }
        })

    });
    app.use(express.static(__dirname + "/public/"));

    app.get('/download', function(req, res){ 
        res.download('converted.json'); 
    }); 


    app.get('/',function(req,res){
		res.sendFile(__dirname + "/public/index.html");
	});

    app.listen('3000', function(){
        console.log('running on 3000...');
    });
